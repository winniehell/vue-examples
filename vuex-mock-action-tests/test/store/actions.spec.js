import { Store } from "vuex-mock-store"

import shop from "../../api/shop"
import actions from "../../store/actions"

jest.mock("../../api/shop")

const store = new Store()

afterEach(() => store.reset())

describe("actions", () => {
  it("getAllProducts", () => {
    const dummyValue = "Please don't make a server call!"
    jest.spyOn(shop, "getProducts").mockResolvedValue(dummyValue)

    return actions.getAllProducts(store).then(() => {
      expect(store.commit).toHaveBeenCalledTimes(2)
      expect(store.commit).toHaveBeenCalledWith("REQUEST_PRODUCTS")
      expect(store.commit).toHaveBeenCalledWith("RECEIVE_PRODUCTS", dummyValue)
    })
  })
})
