import shop from "../api/shop"

export default {
  getAllProducts({ commit }) {
    commit("REQUEST_PRODUCTS")
    return shop.getProducts().then(products => {
      commit("RECEIVE_PRODUCTS", products)
    })
  }
}
